
#include "Arduino.h"

#ifndef _BUT_LoRa_H_
#define _BUT_LoRa_H_

#define BAND_433 433
#define BAND_868 868
#define CONFIRMED_TX 1
#define UNCONFIRMED_TX 0
#define OTAA_JOIN 0
#define ABP_JOIN 1
#define VOLTAGE_PIN 12
#define LORA_RESET_PIN 30

#define GFSK_NONE (char *) "none"
#define GFSK_1_0 (char *) "1.0"
#define GFSK_0_5 (char *) "0.5"
#define GFSK_0_3 (char *) "0.3"

#define FREQ_250 (char *) "250"
#define FREQ_125 (char *) "125"
#define FREQ_62_5 (char *) "62.5"
#define FREQ_31_3 (char *) "31.3"
#define FREQ_15_6 (char *) "15.6"
#define FREQ_7_8 (char *) "7.8"
#define FREQ_3_9 (char *) "3.9"
#define FREQ_200 (char *) "200"
#define FREQ_100 (char *) "100"
#define FREQ_50 (char *) "50"
#define FREQ_25 (char *) "25"
#define FREQ_12_5 (char *) "12.5"
#define FREQ_6_3 (char *) "6.3"
#define FREQ_3_1 (char *) "3.1"
#define FREQ_166_7 (char *) "166.7"
#define FREQ_83_3 (char *) "83.3"
#define FREQ_41_7 (char *) "41.7"
#define FREQ_20_8 (char *) "20.8"
#define FREQ_10_4 (char *) "10.4"
#define FREQ_5_2 (char *) "5.2"
#define FREQ_2_6 (char *) "2.6"

enum Modulation {
    LORA = 0,
    FSK = 1
};

enum FreqPlan {
    EU_SINGLE_CHANNEL = 0,
    BUT_DEFAULT = 1,
    BUT_FULL = 2
};

enum ResponseTypes {
    OK = 0,
    ERR = 1,
    BUSY = 2,
    TIMEOUT = 3
};

enum PowerIndex {
    PWRIDX_0 = 0,
    PWRIDX_1 = 1,
    PWRIDX_2 = 2,
    PWRIDX_3 = 3,
    PWRIDX_4 = 4,
    PWRIDX_5 = 5
};

enum DataRate {
    DR_0 = 0,
    DR_1 = 1,
    DR_2 = 2,
    DR_3 = 3,
    DR_4 = 4,
    DR_5 = 5,
    DR_6 = 6,
    DR_7 = 7
};

enum Channels {
    CHANNEL_0 = 0,
    CHANNEL_1 = 1,
    CHANNEL_2 = 2,
    CHANNEL_3 = 3,
    CHANNEL_4 = 4,
    CHANNEL_5 = 5,
    CHANNEL_6 = 6,
    CHANNEL_7 = 7,
    CHANNEL_8 = 8,
    CHANNEL_9 = 9,
    CHANNEL_10 = 10,
    CHANNEL_11 = 11,
    CHANNEL_12 = 12,
    CHANNEL_13 = 13,
    CHANNEL_14 = 14,
    CHANNEL_15 = 15
};

enum SpreadingFactor {
    SF_7 = 7,
    SF_8 = 8,
    SF_9 = 9,
    SF_10 = 10,
    SF_11 = 11,
    SF_12 = 12
};

enum CodingRate {
    CR_4_5 = 5,
    CR_4_6 = 6,
    CR_4_7 = 7,
    CR_4_8 = 8
};

enum BandWidth{
    BW_125 = 125,
    BW_250 = 250,
    BW_500 = 500
};

struct MacStatus {
    bool joinStatus;
    uint8_t macState;
    bool arpStatus;
    bool adrStatus;
    bool silentStatus;
    bool macPauseStatus;
    bool linkCheckStatus;
    bool channelsUpdated;
    bool outputPowerUpdated;
    bool nbRepUpdated;
    bool prescalerUpdated;
    bool Rx2WindowUpdated;
    bool txTimingUpdated;
    bool rejoinNeeded;
};

class but_lora {
    public:
    
        /*
        Konstruktor třídy
        Stream& modemSerial – sériové rozhraní modulu
        Stream& debugSerial – sériové rozhraní počítače
        char *buffer – ukazatel na buffer pro ukládání přijatých příkazů a zpráv
        size_t size – velikost bufferu
        */
        but_lora(Stream& modemSerial, Stream& debugSerial, char *buffer, size_t size);

        /*
        Konstruktor třídy bez debugovacího rozhraní
        Stream& modemSerial – sériové rozhraní modulu
        char *buffer – ukazatel na buffer pro ukládání přijatých příkazů a zpráv
        size_t size – velikost bufferu
        */
        but_lora(Stream& modemSerial, char *buffer, size_t size);
        
        /*
        Funkce pro uspání modulu
        sys sleep
        uint32_t time - doba spánku v ms
        */
        bool systemSleep(uint32_t time);

        /*
        Funkce pro restart systému
        sys reset
        */
        bool systemReboot();

        /*
        Funkce nastaví EEPROM modulu na tovární hodnoty
        sys factoryRESET
        */
        bool systemFactoryReset();

        /*
        Funkce pro získání aktuální verze systému
        sys get ver
        */
        bool systemGetVersion();

        /*
        Funkce pro získání HW EUI
        sys get hweui
        char *eui - buffer do kterého bude EUI uloženo
        */
        bool systemGetHWEUI(char *eui);

        /*
        Funkce pro reset MAC vrstvy na výchozí hodnoty
        mac reset
        uint16_t band - frekvenční pásmo
        */
        bool macReset(uint16_t band);

        /*
        Funkce pro přenos zpráv skrze MAC vrstvu
        mac tx
        uint8_t type- potvrzovaný/nepotvrzovaný přenos
        uint8_t portNumber - číslo portu
        char *data - data k odeslání
        */
        bool macTx(uint8_t type, uint8_t portNumber, char *data);

        /*
        Funkce umožňující čekání na odpověď po vyslání zprávy
        uint32_t timeout - doba čekání
        */
        bool macWaitForMessage(uint32_t timeout);

        /*
        Fukce pro připojení do LoRaWAN sítě
        mac join
        uint8_t mode - mód aktivace ABP/OTAA
        */
        bool macJoin(uint8_t mode);

        /*
        Funkce pro uložení nastavení do EEPROM
        mac save
        */
        bool macSave();

        /*
        Funkce pro aktivaci MAC vysílání při po přijetí silent příkazu
        mac forceENABLE
        */
        bool macForceEnable();

        /*
        Funkce pro pozastavení MAC stacku
        mac pause
        uint32_t *time - čas po který bude MAC vrstva pozastavena
        */
        bool macPause(uint32_t *time);

        /*
        Funkce pro obnovení funkce MAC vrstvy
        mac resume
        */
        bool macResume();

        /*
        Funkce pro nastavení Device Address
        mac set devaddr
        char *address - device addres
        */
        bool macSetDevAddr(char *address);

        /*
        Funkce pro nastavení Device EUI
        mac set deveui
        char *devEUI- device EUI
        */
        bool macSetDevEUI(char *devEUI);

        /*
        Funkce pro nastavení Application EUI
        mac set appeui
        char *appEUI - application EUI
        */
        bool macSetApplicationEUI(char *appEUI);

        /*
        Funkce pro nastavení Network Session Key
        mac set nwkskey
        char *nwkskey - network session key
        */
        bool macSetNetworkKey(char *nwkskey);

        /*
        Funkce pro nastavení Appplication Session Key
        mac set appskey
        char *appskey - application session key
        */
        bool macSetApplicatioSessionKey(char *appskey);

        /*
        Funkce pro nastavení Application Key
        mac set appkey
        char *appkey - application key
        */
        bool macSetApplicationKey(char *appkey);

        /*
        Funkce pro nastavení vysílacího výkonu MAC vrstvy
        mac set pwridx
        PowerIndex pwridx - index vysílacího výkonu
        */
        bool macSetPowerIndex(PowerIndex pwridx);

        /*
        Funkce pro nastavení přenosové rychlosti
        mac set dr
        DataRate dr - hodnota přenosové rychlosti
        */
        bool macSetDataRate(DataRate dr);

        /*
        Funkce pro nastavení Adaptivní přenosové rychlosti
        mac set adr
        bool enabled - aktivace/deaktivace
        */
        bool macSetAdaptiveDataRate(bool enabled);

        /*
        Funkce pro nastavení počtu retransmisí
        mac set retx
        uint8_t retxnb - počet retransmisí
        */
        bool macSetMacRetransmissions(uint8_t retxnb);

        /*
        Funkce pro nastavení intervalu kontroly linky 
        mac set linkchk
        uint16_t interval - interval kontroly
        */
        bool macSetLinkCheckInterval(uint16_t interval);

        /*
        Funkce pro nastavení zpoždění okna RX1
        mac set rxdelay1
        uint16_t delay - doba zpoždění
        */
        bool macSetRxWindow1Delay(uint16_t delay);

        /*
        Funkce pro nastavení automatického odpovídání
        mac set ar
        bool enabled - aktivace/deaktivace
        */
        bool macSetAutomaticReply(bool enabled);

        /*
        Funkce pro nastavení zpoždění a přenosové rychlosti okna RX2
        mac set rx2
        DataRate dr - přenosová rachlost
        uint32_t frequency - frekvence
        */
        bool macSetRxWindow2DRFreq(DataRate dr, uint32_t frequency);

        /*
        Funkce pro nastavení frekvence kanálu
        mac set ch freq
        Channels channelID - id kanálu
        uint32_t frequency - frekvence
        */
        bool macSetChannelFrequency(Channels channelID, uint32_t frequency);

        /*
        Funkce pro nastavení duty cycle kanálu
        mac set ch dcycle
        Channels channelID - id kanálu
        uint16_t dutyCycle - duty cycle
        */
        bool macSetChannelDutyCycle(Channels channelID, uint16_t dutyCycle);

        /*
        Funkce pro nastavení rozsahu přenosové rychlosti kanálu
        mac set ch drrange
        Channels channelID - id kanálu
        uint8_t min - minimální přenosová rychlost
        uint8_t max - maximální přenosová rychlost
        */
        bool macSetChannelDataRateRange(Channels channelID, uint8_t min, uint8_t max);

        /*
        Funkce pro aktivaci/deaktivaci kanálu
        mac set ch status
        Channels channelID - id kanálu
        bool enabled - aktivace/deaktivace
        */
        bool macSetChannelStatus(Channels channelID, bool enabled);

        /*
        Funkce pro nastavení uplink frame counteru
        mac set upctr
        uint32_t count - hodnota čítače
        */
        bool macSetUplinkCounter(uint32_t count);

        
        /*
        Funkce pro nastavení downlink frame counteru
        mac set dnctr
        uint32_t count - hodnota čítače
        */
        bool macSetDownlinkCounter(uint32_t count);

        /*
        Funkce pro získání Device Address
        mac get devaddr
        char *address - získaná adresa
        */
        void macGetDeviceAddress(char *address);

        /*
        Funkce pro získání Device EUI
        mac get deveui
        mac get deveui - získaná hodnota device EUI
        */
        void macGetDeviceEUI(char *devEUI);

        /*
        Funkce pro získání Application EUI
        mac get appeui
        char *appEUI - získaná hodnota application EUI
        */
        void macGetApplicationEUI(char *appEUI);

        /*
        Funkce pro získání hodnoty přenosové rychlosti
        mac get dr
        */
        uint8_t macGetDataRate();

        /*
        Funkce pro získání frwekvence komunikačního pásma
        mac get band
        */
        uint16_t macGetBand();

        /*
        Funkce pro zjištění hodnoty indexu vysílacího výkonu MAC vrstvy
        mac get pwridx
        */
        uint8_t macGetPowerIndex();

        /*
        Funkce pro zjištění stavu adaptivního přenosu
        mac get adr
        */
        bool macGetAdaptiveDataRate();

        /*
        Funkce pro zjištění počtu retransmisí
        mac get retx
        */
        uint8_t macGetRetransmissions();

        /*
        Funkce pro zjištění zpoždění okna RX1
        mac get rxdelay1
        */
        uint16_t macGetRxWindow1Delay();

        /*
        Funkce pro zjištění zpoždění okna RX2
        mac get rxdelay2
        */
        uint16_t macGetRxWindow2Delay();

        /*
        Funkce pro zjištění stavu automatických odpovědí
        mac get ar
        */
        bool macGetAutomaticReply();

        /*
        Funkce pro zjištění přenosové rychlosti a frekvence okna RX2
        mac get rx2
        uint16_t frequencyBand - požadované frekvenční pásmo
        uint8_t *dataRate - získaná přenosová rychlost
        uint32_t *frequency - získaná frekvence
        */
        bool macGetRxWindow2DataRate(uint16_t frequencyBand, uint8_t *dataRate, uint32_t *frequency);
        
        /*
        Funkce pro získání hodnoty předděličky duty cycle
        mac get dcycleps
        */
        uint16_t macGetDutyCycleprescaler();

        /*
        Funkce pro získání demodulační hranice
        mac get mrgn
        */
        uint8_t macGetDemodulationMargin();

        /*
        Funkce pro zjištění hraničních bran v dosahu
        mac get gwnb
        */
        uint8_t macGetNumberOfGateways();

        /*
        Funkce pro zjištění stavu MAC vrstvy
        mac get status
        */
        MacStatus macGetStatus();

        /*
        Funkce pro zjištění frekvence kanálu
        mac get ch freq
        Channels channelID - id kanálu
        */
        uint32_t macGetChannelFrequency(Channels channelID);

        /*
        Funkce pro zjištění duty cycle kanálu
        mac get ch dcycle
        Channels channelID - id kanálu
        */
        uint16_t macGetChannelDutyCycle(Channels channelID);

        /*
        Funkce pro zjištění přenosové rychlosti kanálu
        mac get ch drrange
        Channels channelID - id kanálu
        */
        void macGetChannelDataRate(Channels channelID, uint8_t *maxDr, uint8_t *minDr);

        /*
        Funkce pro zjištěí stavu kanálu
        mac get ch status
        Channels channelID - id kanálu
        */
        bool macGetChannelStatus(Channels channelID);

        /*
        Funkce pro zapnutí rx módu fyzické vrstvy
        radio rx
        uint16_t windowSize - velikost okna
        char *data - získaná data
        */
        bool radioRx(uint16_t windowSize, char *data);

        /*
        Funkce pro odeslání dat na fyzické vrstvě
        radio tx
        char *data - data k odeslání
        */
        bool radioTx(char *data);

        /*
        Funkce pro spuštění nepřetržitého vysílání
        radio cw
        bool enabled - povoleno/zakázáno
        */
        bool radioContinuousWave(bool enabled);

        /*
        Funkce pro nastavení Gausovského filtru
        radio set bt
        char *gfbt - hodnota tvaru
        */
        bool radioSetGFSK(char *gfbt);

        /*
        Funkce pro nastavení modulace
        radio set mod
        Modulation m - typ modulace
        */
        bool radioSetMod(Modulation m);

        /*
        Funkce pro nastavení frekvence fyzické vrstvy
        radio set freq
        uint32_t frequency - frekvence
        */
        bool radioSetFrequency(uint32_t frequency);

        /*
        Funkce pro nastavení ŕovně vysílacího výkonu
        radio set pwr
        int8_t power - vysílací výkon
        */
        bool radioSetPower(int8_t power);

        /*
        Funkce pro nastavení činitele rozprostření
        radio set sf sf
        SpreadingFactor sf - hodnota činitele rozprostření
        */
        bool radioSetSpreadingFactor(SpreadingFactor sf);

        /*
        Funkce pro nastavení automatické korekce frekvence
        radio set afcbw
        char *frequencyCorrection - hodnota frekvenční korekce
        */
        bool radioAutFreqCorrection(char *frequencyCorrection);

        /*
        Funkce pro nastavení šířka pásma v rx módu
        radio set rxbw
        char *bandwidth - šířka pásma
        */
        bool radioSetRxbandWidth(char *bandwidth);

        /*
        Funkce pro nsatvení rychlosti modulace FSK
        radio set bitrate
        uint32_t bitrate - rychlost modulace
        */
        bool radioSetFSKBitrate(uint32_t bitrate);

        /*
        Funkce pro nastavení odchylky frekvence
        radio set fdev
        uint32_t deviation - hodnota odchylky
        */
        bool radioSetFrequencyDeviation(uint32_t deviation);

        /*
        Funkce pro nastavení délky preambule
        radio set prlen
        uint16_t length- délka preambule
        */
        bool radioSetPreambleLength(uint16_t length);

        /*
        Funkce pro nastavení CRC
        radio set crc
        bool enabled - povoleno/zakázáno
        */
        bool radioSetCRC(bool enabled);

        /*
        Funkce pro nastavení invertované IQ
        radio set iqi
        bool enabled - povoleno/zakázáno
        */
        bool radioSetIQI(bool enabled);

        /*
        Funkce pro nastavení kódovacího poměru
        radio set cr
        CodingRate cr - kódovací poměr
        */
        bool radioSetCodingRate(CodingRate cr);

        /*
        Funkce pro nastavení hodnoty watchdog časovače
        radio set wdt
        uint32_t timer - hodnota časovače
        */
        bool radioSetWatchdog(uint32_t timer);

        /*
        Funkce pro nastavení synchronzačního slova
        radio set sync
        char *syncWord - synchronizační slovo
        */
        bool radioSetSync(char *syncWord);

        /*
        Funkce pro nastavení šířky pásma
        radio set bw
        BandWidth bw - šířka pásma
        */
        bool radioSetBandwidth(BandWidth bw);

        /*
        Funkce pro zjištění nastavení FSK modulace
        radio get bt
        char *bt - získaná hodnota fsk tvarovače
        */
        void radioGetGFSK(char *bt);

        /*
        Funkce pro zjištění použité modulace
        radio get mod
        */
        Modulation radiogetMod();

        /*
        Funkce pro zjištění frekvence rádiového rozhraní
        radio get freq
        */
        uint32_t radioGetFrequency();

        /*
        Funkce pro zjištění vysílacího výkonu rádiového rozhraní
        radio get pwr
        */
        int8_t radioGetPower();

        /*
        Funkce pro zjištění SF rádiového rozhraní
        radio get sf
        */
        uint8_t radioGetSpreadingFactor();

        /*
        Funkce pro zjištění frekvenční korekce
        radio get afcbw
        char *frequencyCorrection - hodnota frekvenční korekce
        */
        void radioGetAutFreqCorrection(char *frequencyCorrection);

        /*
        Funkce pro zjištění šířky ásma v rx módu
        radio get rxbw
        char *bandwidth - získaná hodnota šířky pásma
        */
        void radioGetRxbandWidth(char *bandwidth);

        /*
        Funkce pro zjištění přenosové rychlosti FSK modulace
        radio get bitrate
        */
        uint16_t radioGetBitrate();

        /*
        Funkce pro zjištění frekvenční odchylky
        radio get fdev
        */
        uint16_t radioGetFrequencyDeviation();

        /*
        Funkce pro zjištění délky preambule
        radio get prlen
        */
        uint16_t radioGetPreambleLength();

        /*
        Funkce pro zjištění hodnoty CRC
        radio get crc
        */
        bool radioGetCRC();

        /*
        Funkce pro zjištění hodnoty inverze IQ
        radio get iqi
        */
        bool radioGetIQI();

        /*
        Funkce pro zjištění hodnoty kódovacího poměru
        radio get cr
        */
        uint8_t radioGetcodingRate();

        /*
        Funkce pro zjištění hodnoty watchdog časovače
        radio get wdt
        */
        uint32_t radioGetWatchdog();

        /*
        Funkce pro zjištění šířky pásma
        radio get wdt
        */
        uint16_t radioGetBandWidth();

        /*
        Funkce pro zjištění hodnoty SNR
        radio get snr
        */
        int16_t radioGetSNR();

        void setFrequencyPlan(FreqPlan fp,  DataRate dr);
        
    
    private:
        uint8_t _enableDebug = false;
        Stream &_serial;
        char *_buffer;
        size_t _size;
        Stream &_debug;
        void initPins();
        char *hexCharToBinString(char hexCh);
        char hexCharToAscii(char *response, uint16_t index);
        void sendRawCommand(char *command);
        int8_t parseResponse(char *response, uint32_t timeout);
        void readResponse(char *buffer, uint32_t timeout);
        void cleanBuffer(char *buffer, size_t size);
};

#endif