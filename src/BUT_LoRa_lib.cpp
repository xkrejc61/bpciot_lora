
#include "Arduino.h"
#include "../lib/BUT_LoRa_lib.h"

#define RX_TIMEOUT 300

but_lora::but_lora(Stream& modemSerial, Stream& debugSerial, char *buffer, size_t size): _serial(modemSerial), _buffer(buffer), _debug(debugSerial){
    _serial.setTimeout(100);
    _debug.setTimeout(100);
    _enableDebug = true;
    _size = size;
    initPins();
}

but_lora::but_lora(Stream& modemSerial, char *buffer, size_t size): _serial(modemSerial), _buffer(buffer), _debug(modemSerial) {
    _serial.setTimeout(100);
    _size = size;
    initPins();
}

void but_lora::initPins(){
    pinMode(VOLTAGE_PIN, OUTPUT);
    digitalWrite(VOLTAGE_PIN, HIGH);
    pinMode(LORA_RESET_PIN, OUTPUT);
    delay(500);
    digitalWrite(LORA_RESET_PIN, LOW);
}

void but_lora::sendRawCommand(char *command){
    if(_enableDebug) {
        _debug.print("<< ");
        _debug.println(command);
    }
    while(_serial.available()) {
        _serial.read();
        delay(1);
    }
    _serial.println(command);
}

void but_lora::cleanBuffer(char *buffer, size_t size){
    for(size_t i = 0; i < size; i++){
        buffer[i] = 0;
    }
}

void but_lora::readResponse(char *buffer, uint32_t timeout){
    cleanBuffer(buffer, _size);
    uint32_t start = millis();  
    while(!_serial.available()){
        if(millis() - start > timeout){
            if(_enableDebug){
                _debug.println("No response from module, Timeout ERROR!");
            }
            return;
        }
    }
    size_t index = 0;
    char rxChar = '\0';
    do{
        if(_serial.available()){
            rxChar = _serial.read();
            buffer[index++] = rxChar;
        }
    } while(rxChar != '\n');

    if(_enableDebug){
        _debug.print(">> ");
        _debug.print(buffer);
    }
}

int8_t but_lora::parseResponse(char *response, uint32_t timeout){
    readResponse(response, timeout);
    if(strstr(response, "ok") != NULL || strstr(response, "RN2483 ") != NULL){
        return OK;
    }
    if(strstr(response, "invalid_param") != NULL){
        return ERR;
    }
    if(strstr(response, "busy") != NULL){
        return BUSY;
    }
    if(strlen(response) == 0){
        return TIMEOUT;
    }
    return ERR;
}

bool but_lora::systemSleep(uint32_t time){
    sprintf(_buffer, "sys sleep %lu", time);
    sendRawCommand(_buffer);
    return parseResponse(_buffer, time + RX_TIMEOUT) != OK ? false : true;
}

bool but_lora::systemReboot(){
    sendRawCommand((char *) "sys reset");
    return parseResponse(_buffer, RX_TIMEOUT) != OK ? false : true;
}

bool but_lora::systemFactoryReset(){
    sendRawCommand((char *) "sys factoryRESET");
    return parseResponse(_buffer, 5000) != OK ? false : true;
}

bool but_lora::systemGetVersion(){
    sendRawCommand((char *) "sys get ver");  
    return parseResponse(_buffer, RX_TIMEOUT) != OK ? false : true;
}

bool but_lora::systemGetHWEUI(char *eui){
    sendRawCommand((char *) "sys get hweui");
    readResponse(_buffer, RX_TIMEOUT);
    if(strlen(_buffer) > 0){
        strncpy(eui, _buffer, 16);
        eui[16] = '\0';
        return true;      
    }
    return false;
}

bool but_lora::macReset(uint16_t band){
    sprintf(_buffer, "mac reset %u", band);
    sendRawCommand(_buffer);
    return parseResponse(_buffer, RX_TIMEOUT) != OK ? false : true;
}

bool but_lora::macTx(uint8_t type, uint8_t portNumber, char *data){
    char typeVal[6];
    if(portNumber < 1 || portNumber > 223){
        if(_enableDebug){
            _debug.println("Wrong port number, ERROR");
        }
        return false;
    }
    if(type == CONFIRMED_TX){
        strcpy(typeVal, (char *) "cnf");
    } else if (type == UNCONFIRMED_TX){
        strcpy(typeVal, (char *) "uncnf");
    } else {
        if(_enableDebug){
            _debug.println("Wrong payload type, ERROR");
        }
        return false;
    }
    uint16_t length = strlen(data);
    char *messageBuffer = new char [ 2 * length + 40];

    sprintf(messageBuffer, "mac tx %s %u ", typeVal, portNumber);
    uint16_t offset = strlen(messageBuffer);
    uint16_t i, j;
    for(i = 0, j = 0; i < length; i++, j+=2){
        sprintf((char *) messageBuffer + j + offset, "%02X", data[i]);
    }
    sendRawCommand(messageBuffer);
    delete(messageBuffer);
    return parseResponse(_buffer, RX_TIMEOUT) != OK ? false : true;
}

bool but_lora::macWaitForMessage(uint32_t timeout){
    readResponse(_buffer, 30000);
    if(strstr(_buffer,  "mac_tx_ok") != NULL) return true;
    if(strstr(_buffer, "mac_rx") != NULL) return true;
    return false;
}

bool but_lora::macJoin(uint8_t mode){
    char joinMode[6] = {0};
    if(mode == OTAA_JOIN){
        strcpy(joinMode, (char *) "otaa");
    } else if (mode == ABP_JOIN) {
        strcpy(joinMode, (char *) "abp");
    } else {
        if(_enableDebug){
            _debug.println("Wrong join mode, ERROR");
        }
        return false;
    }
    sprintf(_buffer, "mac join %s", joinMode);
    sendRawCommand(_buffer);
    if(parseResponse(_buffer, RX_TIMEOUT) != OK) return false;
    readResponse(_buffer, 5000);
    if(strstr(_buffer, "accepted") != NULL) return true;
    return false;
}

bool but_lora::macSave(){
    sendRawCommand((char *) "mac save");
    return parseResponse(_buffer, 10 * RX_TIMEOUT) != OK ? false : true;
}

bool but_lora::macForceEnable(){
    sendRawCommand((char *) "mac forceENABLE");
    return parseResponse(_buffer, RX_TIMEOUT) != OK ? false : true;
}

bool but_lora::macPause(uint32_t *time){
    sendRawCommand((char *) "mac pause");
    readResponse(_buffer, RX_TIMEOUT);
    for(uint16_t i = 0; i < strlen(_buffer) - 2; i++){
        if(!isdigit(_buffer[i])){
            return false;
        }
    }
    *time = atol(_buffer);
    return true;
}

bool but_lora::macResume(){
    sendRawCommand((char *) "mac resume");
    return parseResponse(_buffer, RX_TIMEOUT) != OK ? false : true;
}

bool but_lora::macSetDevAddr(char *address){
    sprintf(_buffer, "mac set devaddr %s", address);
    sendRawCommand(_buffer);
    return parseResponse(_buffer, RX_TIMEOUT) != OK ? false : true;
}

bool but_lora::macSetDevEUI(char *devEUI){
    sprintf(_buffer, "mac set deveui %s", devEUI);
    sendRawCommand(_buffer);
    return parseResponse(_buffer, RX_TIMEOUT) != OK ? false : true;
}

bool but_lora::macSetApplicationEUI(char *appEUI){
    sprintf(_buffer, "mac set appeui %s", appEUI);
    sendRawCommand(_buffer);
    return parseResponse(_buffer, RX_TIMEOUT) != OK ? false : true;
}

bool but_lora::macSetNetworkKey(char *nwkskey){
    sprintf(_buffer, "mac set nwkskey %s", nwkskey);
    sendRawCommand(_buffer);
    return parseResponse(_buffer, RX_TIMEOUT) != OK ? false : true;
}

bool but_lora::macSetApplicatioSessionKey(char *appskey){
    sprintf(_buffer, "mac set appskey %s", appskey);
    sendRawCommand(_buffer);
    return parseResponse(_buffer, RX_TIMEOUT) != OK ? false : true;
}

bool but_lora::macSetApplicationKey(char *appkey){
    sprintf(_buffer, "mac set appkey %s", appkey);
    sendRawCommand(_buffer);
    return parseResponse(_buffer, RX_TIMEOUT) != OK ? false : true;
}

bool but_lora::macSetPowerIndex(PowerIndex pwridx){
    sprintf(_buffer, "mac set pwridx %d", pwridx);
    sendRawCommand(_buffer);
    return parseResponse(_buffer, RX_TIMEOUT) != OK ? false : true;
}

bool but_lora::macSetDataRate(DataRate dr){
    sprintf(_buffer, "mac set dr %d", dr);
    sendRawCommand(_buffer);
    return parseResponse(_buffer, RX_TIMEOUT) != OK ? false : true;
}

bool but_lora::macSetAdaptiveDataRate(bool enabled){
    sprintf(_buffer, "mac set adr %s", enabled ? "on" : "off");
    sendRawCommand(_buffer);
    return parseResponse(_buffer, RX_TIMEOUT) != OK ? false : true;
}

bool but_lora::macSetMacRetransmissions(uint8_t retxnb){
    sprintf(_buffer, "mac set retx %u", retxnb);
    sendRawCommand(_buffer);
    return parseResponse(_buffer, RX_TIMEOUT) != OK ? false : true;
}

bool but_lora::macSetLinkCheckInterval(uint16_t interval){
    sprintf(_buffer, "mac set linkchk %u", interval);
    sendRawCommand(_buffer);
    return parseResponse(_buffer, RX_TIMEOUT) != OK ? false : true;
}

bool but_lora::macSetRxWindow1Delay(uint16_t delay){
    sprintf(_buffer, "mac set rxdelay1 %u", delay);
    sendRawCommand(_buffer);
    return parseResponse(_buffer, RX_TIMEOUT) != OK ? false : true;  
}

bool but_lora::macSetAutomaticReply(bool enabled){
    sprintf(_buffer, "mac set ar %s", enabled ? "on" : "off");
    sendRawCommand(_buffer);
    return parseResponse(_buffer, RX_TIMEOUT) != OK ? false : true; 
}

bool but_lora::macSetRxWindow2DRFreq(DataRate dr, uint32_t frequency){
    sprintf(_buffer, "mac set rx2 %d %lu", dr, frequency);
    sendRawCommand(_buffer);
    return parseResponse(_buffer, RX_TIMEOUT) != OK ? false : true; 
}

bool but_lora::macSetChannelFrequency(Channels channelID, uint32_t frequency){
    sprintf(_buffer, "mac set ch freq %d %lu", channelID, frequency);
    sendRawCommand(_buffer);
    return parseResponse(_buffer, RX_TIMEOUT) != OK ? false : true;
}

bool but_lora::macSetChannelDutyCycle(Channels channelID, uint16_t dutyCycle){
    sprintf(_buffer, "mac set ch dcycle %d %d", channelID, dutyCycle);
    sendRawCommand(_buffer);
    return parseResponse(_buffer, RX_TIMEOUT) != OK ? false : true;
}

bool but_lora::macSetChannelDataRateRange(Channels channelID, uint8_t min, uint8_t max){
    sprintf(_buffer, "mac set ch drrange %d %d %d", channelID, min, max);
    sendRawCommand(_buffer);
    return parseResponse(_buffer, RX_TIMEOUT) != OK ? false : true;
}

bool but_lora::macSetChannelStatus(Channels channelID, bool enabled){
    sprintf(_buffer, "mac set ch status %d %s", channelID, (enabled ? "on" : "off"));
    sendRawCommand(_buffer);
    return parseResponse(_buffer, RX_TIMEOUT) != OK ? false : true;
}

bool but_lora::macSetUplinkCounter(uint32_t count){
    sprintf(_buffer, "mac set upctr %lu", count);
    sendRawCommand(_buffer);
    return parseResponse(_buffer, RX_TIMEOUT) != OK ? false : true;
}

bool but_lora::macSetDownlinkCounter(uint32_t count){
    sprintf(_buffer, "mac set dnctr %lu", count);
    sendRawCommand(_buffer);
    return parseResponse(_buffer, RX_TIMEOUT) != OK ? false : true;
}

void but_lora::macGetDeviceAddress(char *address){
    sendRawCommand((char *) "mac get devaddr");
    readResponse(_buffer, RX_TIMEOUT);
    strncpy(address, _buffer, 8);
    address[9] = '\0';
}

void but_lora::macGetDeviceEUI(char *devEUI){
    sendRawCommand((char *) "mac get deveui");
    readResponse(_buffer, RX_TIMEOUT);
    strncpy(devEUI, _buffer, 16);
    devEUI[17] = '\0';
}

void but_lora::macGetApplicationEUI(char *appEUI){
    sendRawCommand((char *) "mac get appeui");
    readResponse(_buffer, RX_TIMEOUT);
    strncpy(appEUI, _buffer, 16);
    appEUI[17] = '\0';
}

uint8_t but_lora::macGetDataRate(){
    sendRawCommand((char *) "mac get dr");
    readResponse(_buffer, RX_TIMEOUT);
    return atoi(_buffer);
}

uint16_t but_lora::macGetBand(){
    sendRawCommand((char *) "mac get band");
    readResponse(_buffer, RX_TIMEOUT);
    return atoi(_buffer);
}

uint8_t but_lora::macGetPowerIndex(){
    sendRawCommand((char *) "mac get pwridx");
    readResponse(_buffer, RX_TIMEOUT);
    return atoi(_buffer);
}

bool but_lora::macGetAdaptiveDataRate(){
    sendRawCommand((char *) "mac get adr");
    readResponse(_buffer, RX_TIMEOUT);
    if(strstr(_buffer, "on") != NULL){
        return true;
    }
    return false;
}

uint8_t but_lora::macGetRetransmissions(){
    sendRawCommand((char *) "mac get retx");
    readResponse(_buffer, RX_TIMEOUT);
    return atoi(_buffer);
}

uint16_t but_lora::macGetRxWindow1Delay(){
    sendRawCommand((char *) "mac get rxdelay1");
    readResponse(_buffer, RX_TIMEOUT);
    return atoi(_buffer);
}

uint16_t but_lora::macGetRxWindow2Delay(){
    sendRawCommand((char *) "mac get rxdelay2");
    readResponse(_buffer, RX_TIMEOUT);
    return atoi(_buffer);
}

bool but_lora::macGetAutomaticReply(){
    sendRawCommand((char *) "mac get ar");
    readResponse(_buffer, RX_TIMEOUT);
    if(strstr(_buffer, "on") != NULL){
        return true;
    }
    return false;
}

bool but_lora::macGetRxWindow2DataRate(uint16_t frequencyBand, uint8_t *dataRate, uint32_t *frequency){
    if(frequencyBand != BAND_868 && frequencyBand != BAND_433){
        if(_enableDebug){
            _debug.println("Wrong frequency band!");
        }
        return false;
    }
    sprintf(_buffer, "mac get rx2 %u", frequencyBand);
    sendRawCommand(_buffer);
    readResponse(_buffer, RX_TIMEOUT);
    size_t spaceIndex = (size_t) (strchr(_buffer, ' ') - _buffer);
    char dr[3] = {0}, freq[10] = {0};

    *dataRate = atoi(strncpy(dr, _buffer, spaceIndex));
    *frequency = atol(strncpy(freq, _buffer + spaceIndex + 1, 9));
    return true;
}

uint16_t but_lora::macGetDutyCycleprescaler(){
    sendRawCommand((char *) "mac get dcycleps");
    readResponse(_buffer, RX_TIMEOUT);
    return atoi(_buffer);
}

uint8_t but_lora::macGetDemodulationMargin(){
    sendRawCommand((char *) "mac get mrgn");
    readResponse(_buffer, RX_TIMEOUT);
    return atoi(_buffer);
}

uint8_t but_lora::macGetNumberOfGateways(){
    sendRawCommand((char *) "mac get gwnb");
    readResponse(_buffer, RX_TIMEOUT);
    return atoi(_buffer);
}

MacStatus but_lora::macGetStatus(){
    MacStatus st;
    sendRawCommand((char *) "mac get status");
    readResponse(_buffer, RX_TIMEOUT);
    uint16_t tempBuffLen = strlen(_buffer) - 2;
    char *tempBuff = new char[tempBuffLen];
    strncpy(tempBuff, _buffer, tempBuffLen);
    tempBuff[tempBuffLen] = '\0';
    uint16_t i = 0;

    cleanBuffer(_buffer, _size);

    for(i = 0; i < strlen(tempBuff) - 1; i++){
        strcat(_buffer, hexCharToBinString(tempBuff[i]));
    };

    strrev(_buffer);

    for(i = 0; i < strlen(tempBuff); i++){
        bool value = tempBuff[i] - '0';
        switch (i)
        {
            case 0:
                st.joinStatus = value;
                break;
            case 1:
                {
                    char macState[4] = {0};
                    strncpy(macState, tempBuff + i, 3);
                    st.macState = (uint8_t) strtol(macState, NULL, 2);
                    i+=2;
                }
                break;
            case 4:
                st.arpStatus = value;
            break;
            case 5:
                st.adrStatus = value;
            break;
            case 6:
                st.silentStatus = value;
            break;
            case 7:
                st.macPauseStatus = value;
            break;
            case 9:
                st.linkCheckStatus = value;
            break;
            case 10:
                st.channelsUpdated = value;
            break;
            case 11:
                st.outputPowerUpdated = value;
            break;
            case 12:
                st.nbRepUpdated = value;
            break;
            case 13:
                st.prescalerUpdated = value;
            break;
            case 14:
                st.Rx2WindowUpdated = value;
            break;
            case 15:
                st.txTimingUpdated = value;
            break;
            case 16:
                st.rejoinNeeded = value;
            break;
        }   
    }
    return st;
}

char *but_lora::hexCharToBinString(char hexCh){
    char *binString = new char[5];
    switch (hexCh)
    {
        case '0':
            strcpy(binString, "0000");
        break;
        case '1':
            strcpy(binString, "0001");
        break;
        case '2':
            strcpy(binString, "0010");
        break;
        case '3':
            strcpy(binString, "0011");
        break;
        case '4':
            strcpy(binString, "0100");
        break;
        case '5':
            strcpy(binString, "0101");
        break;
        case '6':
            strcpy(binString, "0110");
        break;
        case '7':
            strcpy(binString, "0111");
        break;
        case '8':
            strcpy(binString, "1000");
        break;
        case '9':
            strcpy(binString, "1001");
        break;
        case 'A':
            strcpy(binString, "1010");
        break;
        case 'B':
            strcpy(binString, "1011");
        break;
        case 'C':
            strcpy(binString, "1100");
        break;
        case 'D':
            strcpy(binString, "1101");
        break;
        case 'E':
            strcpy(binString, "1110");
        break;
        case 'F':
            strcpy(binString, "1111");
        break;
    }
    binString[4] = '\0';
    return binString;
}

uint32_t but_lora::macGetChannelFrequency(Channels channelID){
    sprintf(_buffer, "mac get ch freq %d", channelID);
    sendRawCommand(_buffer);
    readResponse(_buffer, RX_TIMEOUT);
    return atol(_buffer);
}

uint16_t but_lora::macGetChannelDutyCycle(Channels channelID){
    sprintf(_buffer, "mac get ch dcycle %d", channelID);
    sendRawCommand(_buffer);
    readResponse(_buffer, RX_TIMEOUT);
    return atoi(_buffer);
}

void but_lora::macGetChannelDataRate(Channels channelID, uint8_t *maxDr, uint8_t *minDr){
    sprintf(_buffer, "mac get ch drrange %d", channelID);
    sendRawCommand(_buffer);
    readResponse(_buffer, RX_TIMEOUT);

    size_t spaceIndex = (size_t) (strchr(_buffer, ' ') - _buffer);

    char miDr[3] = {0}, maDr[3] = {0};
    *maxDr = atoi(strncpy(miDr, _buffer, spaceIndex));
    *minDr = atoi(strncpy(maDr, _buffer + spaceIndex + 1, strlen(_buffer) - spaceIndex - 3));
}

bool but_lora::macGetChannelStatus(Channels channelID){
    sprintf(_buffer, "mac get ch status %d", channelID);
    sendRawCommand(_buffer);
    readResponse(_buffer, RX_TIMEOUT);
    if(strstr(_buffer, "on") != NULL){
        return true;
    }
    return false;
}

char but_lora::hexCharToAscii(char *response, uint16_t index){
    uint8_t high = response[index] <= '9' ? response[index] - '0' : response[index] - 'A' + 10;
    uint8_t low = response[++index] <= '9' ? response[index] - '0' : response[index] - 'A'+ 10;
    return (high << 4) | low;
}

bool but_lora::radioRx(uint16_t windowSize, char *data){
    sprintf(_buffer, "radio rx %u", windowSize);
    sendRawCommand(_buffer);
    if(parseResponse(_buffer, RX_TIMEOUT) != OK){
        return false;
    }
    readResponse(_buffer,  windowSize > 0 ? windowSize : UINT32_MAX);

    if(strstr(_buffer, "radio_rx") != NULL){
        char *message = new char[strlen(_buffer) / 2];
        cleanBuffer(message, strlen(_buffer) / 2);
        uint8_t j, i;
        uint8_t spaceIndex = (uint8_t) (strchr(_buffer, ' ') - _buffer) + 2;
        for(i = spaceIndex,  j = 0; i < strlen(_buffer) - 2; i+=2, j++){
          message[j] = hexCharToAscii(_buffer, i);
        }
        strcpy(data, message);
        return true;
    }
    return false;
}

bool but_lora::radioTx(char *data){
    uint16_t length = strlen(data);

    if(length > 255){
        if(_enableDebug){
            _debug.println("Maximum size is 255 bytes!");
            return false;
        }
    }

    char *messageBuffer = new char [ 2 * length + 20];
    sprintf(messageBuffer, "radio tx ");
    uint16_t offset = strlen(messageBuffer);
    uint16_t i, j;
    for(i = 0, j = 0; i < length; i++, j+=2){
        sprintf((char *) messageBuffer + j + offset, "%02X", data[i]);
    }
    sendRawCommand(messageBuffer);
    delete(messageBuffer);
    if(parseResponse(_buffer, RX_TIMEOUT) != OK) return false;
    readResponse(_buffer,  20000);
    if(strstr(_buffer, "radio_tx_ok") != NULL) return true;
    return false;
}

bool but_lora::radioContinuousWave(bool enabled){
    sprintf(_buffer, "radio cw %s", enabled ? "on" : "off");
    sendRawCommand(_buffer);
    return parseResponse(_buffer, RX_TIMEOUT) != OK ? false : true;
}

bool but_lora::radioSetGFSK(char *gfbt){
    sprintf(_buffer, "radio set bt %s", gfbt);
    sendRawCommand(_buffer);
    return parseResponse(_buffer, RX_TIMEOUT) != OK ? false : true;
}
bool but_lora::radioSetMod(Modulation m){
    sprintf(_buffer, "radio set mod %s", m == LORA ? "lora" : "fsk");
    sendRawCommand(_buffer);
    return parseResponse(_buffer, RX_TIMEOUT) != OK ? false : true;
}

bool but_lora::radioSetFrequency(uint32_t frequency){
    sprintf(_buffer, "radio set freq %lu", frequency);
    sendRawCommand(_buffer);
    return parseResponse(_buffer, RX_TIMEOUT) != OK ? false : true;
}

bool but_lora::radioSetPower(int8_t power){
    sprintf(_buffer, "radio set pwr %d", power);
    sendRawCommand(_buffer);
    return parseResponse(_buffer, RX_TIMEOUT) != OK ? false : true;
}
bool but_lora::radioSetSpreadingFactor(SpreadingFactor sf){
    sprintf(_buffer, "radio set sf sf%d", sf);
    sendRawCommand(_buffer);
    return parseResponse(_buffer, RX_TIMEOUT) != OK ? false : true;
}

bool but_lora::radioAutFreqCorrection(char *frequencyCorrection){
    sprintf(_buffer, "radio set afcbw %s", frequencyCorrection);
    sendRawCommand(_buffer);
    return parseResponse(_buffer, RX_TIMEOUT) != OK ? false : true;
}

bool but_lora::radioSetRxbandWidth(char *bandwidth){
    sprintf(_buffer, "radio set rxbw %s", bandwidth);
    sendRawCommand(_buffer);
    return parseResponse(_buffer, RX_TIMEOUT) != OK ? false : true;
}

bool but_lora::radioSetFSKBitrate(uint32_t bitrate){
    if(bitrate < 1 || bitrate > 300000){
        if(_enableDebug){
            _debug.println("Bitrate must be in range from 1 to 300000!");
        }
        return false;
    }
    sprintf(_buffer, "radio set bitrate %lu", bitrate);
    sendRawCommand(_buffer);
    return parseResponse(_buffer, RX_TIMEOUT) != OK ? false : true;
}

bool but_lora::radioSetFrequencyDeviation(uint32_t deviation){
    if(deviation < 1 || deviation > 200000){
        if(_enableDebug){
            _debug.println("Frequency deviation must be in range from 1 to 200000!");
        }
        return false;
    }
    sprintf(_buffer, "radio set fdev %lu", deviation);
    sendRawCommand(_buffer);
    return parseResponse(_buffer, RX_TIMEOUT) != OK ? false : true;
}

bool but_lora::radioSetPreambleLength(uint16_t length){
    sprintf(_buffer, "radio set prlen %u", length);
    sendRawCommand(_buffer);
    return parseResponse(_buffer, RX_TIMEOUT) != OK ? false : true;
}

bool but_lora::radioSetCRC(bool enabled){
    sprintf(_buffer, "radio set crc %s", enabled ? "on" : "off");
    sendRawCommand(_buffer);
    return parseResponse(_buffer, RX_TIMEOUT) != OK ? false : true;
}

bool but_lora::radioSetIQI(bool enabled){
    sprintf(_buffer, "radio set iqi %s", enabled ? "on" : "off");
    sendRawCommand(_buffer);
    return parseResponse(_buffer, RX_TIMEOUT) != OK ? false : true;
}

bool but_lora::radioSetCodingRate(CodingRate cr){
    sprintf(_buffer, "radio set cr 4/%d", cr);
    sendRawCommand(_buffer);
    return parseResponse(_buffer, RX_TIMEOUT) != OK ? false : true;
}

bool but_lora::radioSetWatchdog(uint32_t timer){
    sprintf(_buffer, "radio set wdt %lu", timer);
    sendRawCommand(_buffer);
    return parseResponse(_buffer, RX_TIMEOUT) != OK ? false : true;   
}

bool but_lora::radioSetSync(char *syncWord){
    sprintf(_buffer, "radio set sync %s", syncWord);
    sendRawCommand(_buffer);
    return parseResponse(_buffer, RX_TIMEOUT) != OK ? false : true;
}

bool but_lora::radioSetBandwidth(BandWidth bw){
    sprintf(_buffer, "radio set bw %d", bw);
    sendRawCommand(_buffer);
    return parseResponse(_buffer, RX_TIMEOUT) != OK ? false : true;
}

void but_lora::radioGetGFSK(char *bt){
    sendRawCommand((char *) "radio get bt");
    readResponse(_buffer, RX_TIMEOUT);
    strncpy(bt, _buffer, strlen(_buffer) - 2);
}

Modulation but_lora::radiogetMod(){
    sendRawCommand((char *) "radio get mod");
    readResponse(_buffer, RX_TIMEOUT);
    return strstr(_buffer, "lora") != NULL ? LORA : FSK;
}

uint32_t but_lora::radioGetFrequency(){
    sendRawCommand((char *) "radio get freq");
    readResponse(_buffer, RX_TIMEOUT);
    return atol(_buffer);
}

int8_t but_lora::radioGetPower(){
    sendRawCommand((char *) "radio get pwr");
    readResponse(_buffer, RX_TIMEOUT);
    return atoi(_buffer);
}

uint8_t but_lora::radioGetSpreadingFactor(){
    sendRawCommand((char *) "radio get sf");
    readResponse(_buffer, RX_TIMEOUT);
    /*char temp[3] = {0};
    strncpy(temp, _buffer + 2, strlen(_buffer + 2) - 2);*/
    return atoi(_buffer + 2);
}

void but_lora::radioGetAutFreqCorrection(char *frequencyCorrection){
    sendRawCommand((char *) "radio get afcbw");
    readResponse(_buffer, RX_TIMEOUT);
    strncpy(frequencyCorrection, _buffer, strlen(_buffer) - 2);
}

void but_lora::radioGetRxbandWidth(char *bandwidth){
    sendRawCommand((char *) "radio get rxbw");
    readResponse(_buffer, RX_TIMEOUT);
    strncpy(bandwidth, _buffer, strlen(_buffer) - 2);
}

uint16_t but_lora::radioGetBitrate(){
    sendRawCommand((char *) "radio get bitrate");
    readResponse(_buffer, RX_TIMEOUT);
    return atoi(_buffer);
}

uint16_t but_lora::radioGetFrequencyDeviation(){
    sendRawCommand((char *) "radio get fdev");
    readResponse(_buffer, RX_TIMEOUT);
    return atoi(_buffer);  
}

uint16_t but_lora::radioGetPreambleLength(){
    sendRawCommand((char *) "radio get prlen");
    readResponse(_buffer, RX_TIMEOUT);
    return atoi(_buffer);  
}

bool but_lora::radioGetCRC(){
    sendRawCommand((char *) "radio get crc");
    readResponse(_buffer, RX_TIMEOUT);
    return strstr(_buffer, "on") != NULL ? true : false;
}

bool but_lora::radioGetIQI(){
    sendRawCommand((char *) "radio get iqi");
    readResponse(_buffer, RX_TIMEOUT);
    return strstr(_buffer, "on") != NULL ? true : false;
}

uint8_t but_lora::radioGetcodingRate(){
    sendRawCommand((char *) "radio get cr");
    readResponse(_buffer, RX_TIMEOUT);
    //char temp[2] = {0};
    size_t slashIndex = (size_t) (strchr(_buffer, '/') - _buffer);
    //strncpy(temp, _buffer + slashIndex + 1, 1);
    return atoi(_buffer + slashIndex + 1);
}

uint32_t but_lora::radioGetWatchdog(){
    sendRawCommand((char *) "radio get wdt");
    readResponse(_buffer, RX_TIMEOUT);
    return atol(_buffer);
}

uint16_t but_lora::radioGetBandWidth(){
    sendRawCommand((char *) "radio get bw");
    readResponse(_buffer, RX_TIMEOUT);
    return atoi(_buffer);
}

int16_t but_lora::radioGetSNR(){
   sendRawCommand((char *) "radio get snr");
   readResponse(_buffer, RX_TIMEOUT);
   return atoi(_buffer); 
}

void but_lora::setFrequencyPlan(FreqPlan fp, DataRate dr){
    switch (fp)
    {
        case EU_SINGLE_CHANNEL:
            sprintf(_buffer, "mac set rx2 %d 869525000", dr);
            sendRawCommand(_buffer);
            sendRawCommand((char *) "mac set ch dcycle 0 99");
            sendRawCommand((char *) "mac set ch dcycle 1 65535");
            sendRawCommand((char *) "mac set ch dcycle 2 65535");

        break;
        case BUT_FULL:
            //RX window 2
            sprintf(_buffer, "mac set rx2 %d 869525000", dr);
            sendRawCommand(_buffer);

            //channel 0
            sendRawCommand((char *) "mac set ch dcycle 0 799");

            //channel 1
            sendRawCommand((char *) "mac set ch freq 1 868100000");
            sendRawCommand((char *) "mac set ch drrange 1 0 6");
            sendRawCommand((char *) "mac set ch dcycle 1 799");

            //channel 2
            sendRawCommand((char *) "mac set ch freq 2 868300000");
            sendRawCommand((char *) "mac set ch dcycle 2 799");

            //channel 3
            sendRawCommand((char *) "mac set ch freq 3 868500000");
            sendRawCommand((char *) "mac set ch drrange 3 0 5");
            sendRawCommand((char *) "mac set ch dcycle 3 799");
            sendRawCommand((char *) "mac set ch status 3 on");

            //channel 4
            sendRawCommand((char *) "mac set ch freq 4 867100000");
            sendRawCommand((char *) "mac set ch drrange 4 0 5");
            sendRawCommand((char *) "mac set ch dcycle 4 799");
            sendRawCommand((char *) "mac set ch status 4 on");

            //channel 5
            sendRawCommand((char *) "mac set ch freq 5 867300000");
            sendRawCommand((char *) "mac set ch drrange 5 0 5");
            sendRawCommand((char *) "mac set ch dcycle 5 799");
            sendRawCommand((char *) "mac set ch status 5 on");

            //channel 6
            sendRawCommand((char *) "mac set ch freq 6 867500000");
            sendRawCommand((char *) "mac set ch drrange 6 0 5");
            sendRawCommand((char *) "mac set ch dcycle 6 799");
            sendRawCommand((char *) "mac set ch status 6 on");

            //channel 7
            sendRawCommand((char *) "mac set ch freq 7 867700000");
            sendRawCommand((char *) "mac set ch drrange 7 0 5");
            sendRawCommand((char *) "mac set ch dcycle 7 799");
            sendRawCommand((char *) "mac set ch status 7 on");
        break;
        case BUT_DEFAULT:
            //fix duty cycle - 1% = 0.33% per channel
            sendRawCommand((char *) "mac set ch dcycle 0 299");
            sendRawCommand((char *) "mac set ch dcycle 1 299");
            sendRawCommand((char *) "mac set ch dcycle 2 299");

            //disable non-default channels
            /*sendRawCommand((char *) "mac set ch status 3 on");
            readResponse(_buffer, RX_TIMEOUT);
            sendRawCommand((char *) "mac set ch status 4 on");
            readResponse(_buffer, RX_TIMEOUT);
            sendRawCommand((char *) "mac set ch status 5 on");
            readResponse(_buffer, RX_TIMEOUT);
            sendRawCommand((char *) "mac set ch status 6 on");
            readResponse(_buffer, RX_TIMEOUT);
            sendRawCommand((char *) "mac set ch status 7 on");
            readResponse(_buffer, RX_TIMEOUT);*/
        break;
    }
}





